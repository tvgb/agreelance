from unittest import skip

from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from projects.forms import TaskOfferForm
from projects.models import Project, TaskOffer, Task
from projects.views import project_view, get_user_task_permissions
from user.models import Profile
from django.contrib.auth.models import User
import json
from django.core.exceptions import MultipleObjectsReturned


# Create your tests here.

# output coverage test
class NoTaskOffers(TestCase):
    fixtures = ["test_db_no_accepted_offers.json"]

    def test(self):
        # task_offer_1 = TaskOffer.objects.get(pk=1)
        # task_offer_2 = TaskOffer.objects.get(pk=2)

        task = Task.objects.get(pk=1)

        self.assertIsNone(task.accepted_task_offer())

# output coverage test
class OneTaskOffer(TestCase):
    fixtures = ["test_db.json"]

    def test(self):
        task = Task.objects.get(pk=1)
        task_offer_1 = TaskOffer.objects.get(pk=1)

        self.assertEqual(task_offer_1, task.accepted_task_offer())

# output coverage test
@skip("Fails. Details in the rapport")
class TwoTaskOffers(TestCase):
    # Only returning one task offers, so adding more than two shoul not make a difference
    fixtures = ["test_db_two_accepted_offers.json"]

    def test(self):
        task = Task.objects.get(pk=1)
        task_offer_1 = TaskOffer.objects.get(pk=1)
        failed = False
        try:  # assertRaises did not work for some reason
            task.accepted_task_offer()
        except MultipleObjectsReturned as e:
            print("Exception MultipleObjectsReturned raised")
            print(e)
            failed = True

        if failed is True:
            self.assertTrue(False)  # to show that the test failed. Had to be moved out of the try except block to work

        # self.assertRaises(MultipleObjectsReturned, task.accepted_task_offer())


class EditIntegrationTests(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.client_joe = Client()
        self.client_joe.login(username='joe', password='qwerty123')

        self.client_hp = Client()
        self.client_hp.login(username='harrypotter', password='qwerty123')

        self.project_owner_client = Client()
        self.project_owner_client.login(username='admin', password='qwerty123')

    def testGettingCorrectEditPage(self):
        response = self.project_owner_client.get(reverse('edit_project', kwargs={'project_id': 1}))

        self.assertEqual(response.status_code, 200)

    def testOnlyCreatorCanEdit(self):
        response = self.client_joe.get(reverse('edit_project', kwargs={'project_id': 1}))

        self.assertEqual(response.url, "login.html")  # redirected to login.html

    def testCorrectValuesEdited(self):
        project_id = 1
        project1 = Project.objects.get(pk=project_id)
        tasks1 = project1.tasks.all()
        task1_title = tasks1[0].title
        task1_description = tasks1[0].description
        task1_budget = tasks1[0].budget
        task1_project = tasks1[0].project

        response = self.client_joe.post(
            reverse("edit_project",
                    kwargs={"project_id": project_id}
                    ),
                    {
                        "title": "hei",
                        "description": "og",
                        "category_id": 1,
                        "task_title": ["task11"],
                        "task_description": ["description11"],
                        "task_budget": [11],
                        "project": 1
                    }
        )

        project2 = Project.objects.get(pk=project_id)
        tasks2 = project2.tasks.all()
        self.assertNotEqual(project1.title, project2.title)
        self.assertNotEqual(project1.description, project2.description)
        self.assertNotEqual(project1.category_id, project2.category_id)

        self.assertNotEqual(task1_title, tasks2[0].title)
        self.assertNotEqual(task1_description, tasks2[0].description)
        self.assertNotEqual(task1_budget, tasks2[0].budget)
        self.assertEqual(task1_project, tasks2[0].project)  # should stay the same


class ProjectViewTests(TestCase):
    fixtures = ['seed.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username='joe', password='qwerty123')

        self.project_owner_client = Client()
        self.project_owner_client.login(username='admin', password='qwerty123')

    def test_stored_task_offer_is_as_intended(self):
        project_id = 1
        title = 'This is a test title'
        description = 'This is a test description'
        price = 100
        taskvalue = 1

        response = self.client.post(
            reverse('project_view', kwargs={'project_id': project_id}), {
                'title': title,
                'description': description,
                'price': price,
                'taskvalue': taskvalue,
                'offer_submit': '',
            }
        )

        actual_task_offer = TaskOffer.objects.all().order_by('-id')[0]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(actual_task_offer.title, title)
        self.assertEqual(actual_task_offer.description, description)
        self.assertEqual(actual_task_offer.price, price)
        self.assertEqual(actual_task_offer.offerer, response.context['user'].profile)

    def test_project_owner_accepts_offer_works(self):
        project_id = 1
        status = 'a'
        feedback = 'Test feedback'
        taskofferid = 1

        response = self.project_owner_client.post(
            reverse('project_view', kwargs={'project_id': project_id}), {
                'status': status,
                'feedback': feedback,
                'taskofferid': taskofferid,
                'offer_response': ''
            }
        )

        updated_task_offer = TaskOffer.objects.get(pk=taskofferid)

        self.assertEqual(updated_task_offer.status, status)
        self.assertEqual(updated_task_offer.feedback, feedback)

    def test_project_owner_changes_project_status_works(self):
        project_id = 1
        status = 'i'

        response = self.project_owner_client.post(
            reverse('project_view', kwargs={'project_id': project_id}), {
                'status': status,
                'status_change': ''
            }
        )

        updated_project = Project.objects.get(pk=project_id)

        self.assertEqual(updated_project.status, status)


class GetUserTaskPermissionsTests(TestCase):
    fixtures = ['seed.json']

    def setUp(self):
        self.task = Task.objects.get(pk=1)
        self.project_owner_user = self.task.project.user.user

    def test_project_owner_has_correct_permissions(self):
        permissions = get_user_task_permissions(self.project_owner_user, self.task)

        self.assertTrue(permissions['write'])
        self.assertTrue(permissions['read'])
        self.assertTrue(permissions['modify'])
        self.assertTrue(permissions['owner'])
        self.assertTrue(permissions['upload'])

    def test_user_with_accepted_task_offer_has_correct_permissions(self):
        user = User.objects.all().filter(username='joe')[0]

        permissions = get_user_task_permissions(user, self.task)

        self.assertTrue(permissions['write'])
        self.assertTrue(permissions['read'])
        self.assertTrue(permissions['modify'])
        self.assertFalse(permissions['owner'])
        self.assertTrue(permissions['upload'])

    def test_user_with_no_relation_to_task_offer_has_correct_permissions(self):
        user = User.objects.all().filter(username='harrypotter')[0]

        permissions = get_user_task_permissions(user, self.task)

        self.assertFalse(permissions['write'])
        self.assertFalse(permissions['read'])
        self.assertFalse(permissions['modify'])
        self.assertFalse(permissions['owner'])
        self.assertFalse(permissions['upload'])
        self.assertFalse(permissions['view_task'])


class TaskOfferBlackBox(TestCase):

    def setUp(self):
        self.defaultValues = {
            "title": "the_title",
            "description": "the_description",
            "price": 1
        }

    def testDefaultValues(self):
        form = TaskOfferForm(data=self.defaultValues)
        self.assertTrue(form.is_valid())

    def testNotValidTitle(self):
        with_not_valid_title = self.defaultValues
        not_valid_values = ["", "a" * 201]
        for invalid in not_valid_values:
            with_not_valid_title["title"] = invalid
            form = TaskOfferForm(data=with_not_valid_title)
            self.assertFalse(form.is_valid())

    def testNoDescription(self):
        # No limit on length
        with_no_description = self.defaultValues
        with_no_description["description"] = ""
        form = TaskOfferForm(data=with_no_description)
        self.assertFalse(form.is_valid())

    def testPriceIsNumber(self):
        with_not_number = self.defaultValues
        with_not_number["price"] = "a"
        form = TaskOfferForm(data=with_not_number)
        self.assertFalse(form.is_valid())
