from django.db import models
from user.models import Profile

# Create your models here.

class Chat(models.Model):
    profile_1 = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='chat_profile_1')
    profile_2 = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='chat_profile_2')
    datetime_created = models.DateTimeField(auto_now_add=True)


class ChatMessages(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='chat_message_profile')
    message = models.TextField(max_length=256, blank=False)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)