scrollToBottom()

var roomName = chat_id;

var chatSocket = new WebSocket(
    'ws://' + window.location.host +
    '/ws/chat/' + roomName + '/'
);

chatSocket.onmessage = function(e) {

    const data = JSON.parse(e.data);
    addMessage(data['message'], data['datetime'], data['user_id']);
    
    scrollToBottom()
};

chatSocket.onclose = function(e) {
    console.error('Chat socket closed unexpectedly');
};

document.querySelector('#chat-message-input').focus();
document.querySelector('#chat-message-input').onkeyup = function(e) {
    if (e.keyCode === 13) {  // enter, return
        document.querySelector('#chat-message-submit').click();
    }
};

document.querySelector('#chat-message-submit').onclick = function(e) {
    const messageInputDom = document.querySelector('#chat-message-input');
    const message = messageInputDom.value;
    
    if (message.trim() == "") {
        return
    }

    chatSocket.send(JSON.stringify({
        'message': message,
        'chat_id': chat_id,
    }));

    messageInputDom.value = '';
};

function scrollToBottom() {
    const element = document.getElementById("messages_div");
    element.scrollTop = element.scrollHeight
}

function addMessage(message, timestamp, message_user_id) {
    let message_container_div = document.createElement('DIV');
    message_container_div.classList.add('message_container_div');

    let message_div = document.createElement('DIV');

    let datetime_div = document.createElement('DIV');
    datetime_div.classList.add('datetime')

    if (message_user_id == user_id) {
        message_div.classList.add('right_message');
        datetime_div.classList.add('right_message');
    } else {
        message_div.classList.add('left_message');
        datetime_div.classList.add('left_message');
    }

    message_div.innerHTML = message;
    let datetime = new Date(timestamp * 1000)
    datetime_div.innerHTML = `${datetime.toLocaleTimeString()} ${datetime.toLocaleDateString()}`


    message_container_div.appendChild(message_div);
    message_container_div.appendChild(datetime_div);

    document.getElementById('messages_div').appendChild(message_container_div);
}