from django.shortcuts import render
from .models import Chat, ChatMessages
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core import serializers

from user.models import Profile
from django.shortcuts import render, redirect, get_object_or_404
from django.db import IntegrityError
from .forms import ChatMessageForm
from django.db.models import Q

from django.http import JsonResponse


# Create your views here.
@login_required
def chat(request):

    chats = Chat.objects.all().filter(Q(profile_1_id=request.user.id) | Q(profile_2_id=request.user.id))

    return render(request,
        'chat/chat.html',
        {
            'chats': chats 
        }
    )

@login_required
def new_chat(request, user_1_id, user_2_id):

    if user_1_id == user_2_id:
        return redirect('/')
    

    if request.method == 'POST':
        user_1 = Profile.objects.get(pk=user_1_id)
        user_2 = Profile.objects.get(pk=user_2_id)

        new_chat_entity = Chat()
        new_chat_entity.profile_1 = user_1
        new_chat_entity.profile_2 = user_2

        chats = Chat.objects.all().filter(
            Q (
                Q(profile_1_id=user_1.id),
                Q(profile_2_id=user_2.id)
            ) 
            | 
            Q (
                Q(profile_1_id=user_2.id),
                Q(profile_2_id=user_1.id)
            )
        )

        if len(chats) == 0:
            new_chat_entity.save()
        else:

            return redirect(f'/chat/{chats[0].id}/')
        
    
    return redirect('/chat')

@login_required
def room(request, chat_id):
    chat_messages = ChatMessages.objects.all().filter(chat_id=chat_id)
    chat = Chat.objects.get(pk=chat_id)

    return render(request, 'chat/room.html', {
        'chat_id': chat_id,
        'chat_messages': chat_messages,
        'chat': chat
    })