from django.urls import path
from . import views

urlpatterns = [
    path('', views.chat, name='chat'),
    path('new_chat/<user_1_id>/<user_2_id>', views.new_chat, name='new_chat'),
    path('<chat_id>/', views.room, name='room')
]
