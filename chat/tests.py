from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from user.models import Profile
from chat.models import Chat, ChatMessages
from django.contrib.auth.models import User
import json


# Create your tests here.


class ChatTests(TestCase):

    fixtures = ['test_db.json']

    def setUp(self):
        self.client_joe = Client()
        self.client_joe.login(username='joe', password='qwerty123')

        self.client_hp = Client()
        self.client_hp.login(username='harrypotter', password='qwerty123')

        self.project_owner_client = Client()
        self.project_owner_client.login(username='admin', password='qwerty123')


    def test_chat_retrieves_the_correct_chats(self):
       
        response = self.client_joe.get(reverse('chat'))

        client_id = response.context['user'].id
        
        for chat in response.context['chats']:
            self.assertTrue(chat.profile_1.id == client_id or chat.profile_2.id == client_id)


class NewChatTests(TestCase):

    fixtures = ['test_db.json']

    def setUp(self):
        self.client_joe = Client()
        self.client_joe.login(username='joe', password='qwerty123')

        self.client_hp = Client()
        self.client_hp.login(username='harrypotter', password='qwerty123')

        self.project_owner_client = Client()
        self.project_owner_client.login(username='admin', password='qwerty123')

    def test_user_cannot_create_chat_with_themselves(self):
        
        user_id = 1
        response = self.client_joe.get(reverse('new_chat', kwargs={'user_1_id': user_id, 'user_2_id': user_id}))
        
        self.assertRedirects(response, '/')

    def test_new_chat_only_creates_new_chat_if_it_does_not_already_exist(self):

        user_1_id, user_2_id = 1, 3  # admin, joe
        number_of_initial_chats = len(Chat.objects.all())
        response = self.client_joe.post(reverse('new_chat', kwargs={'user_1_id': user_1_id, 'user_2_id': user_2_id}))
        number_of_chats = len(Chat.objects.all())

        self.assertEquals(number_of_chats, number_of_initial_chats)

    def test_new_chat_works(self):
        user_1_id, user_2_id = 2, 3  # harrypotter, joe
        
        number_of_initial_chats = len(Chat.objects.all())
        response = self.client_joe.post(reverse('new_chat', kwargs={'user_1_id': user_1_id, 'user_2_id': user_2_id}))
        chats = Chat.objects.all()
        number_of_chats = len(chats)

        self.assertNotEquals(number_of_chats, number_of_initial_chats)
        self.assertEquals(user_1_id, chats[number_of_chats - 1].profile_1.id)
        self.assertEquals(user_2_id, chats[number_of_chats - 1].profile_2.id)


class RoomTests(TestCase):
    
    fixtures = ['test_db.json']

    def setUp(self):
        self.client_joe = Client()
        self.client_joe.login(username='joe', password='qwerty123')

        self.client_hp = Client()
        self.client_hp.login(username='harrypotter', password='qwerty123')

        self.project_owner_client = Client()
        self.project_owner_client.login(username='admin', password='qwerty123')

    def test_room_retrieves_all_chat_messages(self):
        chat_id = 1

        response = self.client_joe.get(reverse('room', kwargs={'chat_id': chat_id}))

        self.assertEquals(len(response.context['chat_messages']), 2)