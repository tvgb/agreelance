# chat/consumers.py
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async, close_old_connections
from django.db import connection
from .models import Chat, ChatMessages
from user.models import Profile
from datetime import datetime
import json

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']     
        chat_id = text_data_json['chat_id']
        user_id = self.scope['user'].id

        chat = await self.get_chat(chat_id)
        profile = await self.get_profile(user_id)
        chat_message = await self.save_chat_message(message, chat, profile)

        message_datetime = datetime.timestamp(chat_message.datetime)

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'user_id': self.scope['user'].id,
                'username': self.scope['user'].username,
                'datetime': message_datetime
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps(event))

    
    @database_sync_to_async
    def get_chat(self, chat_id):
        return Chat.objects.get(pk=chat_id)


    @database_sync_to_async
    def get_profile(self, user_id):
        return Profile.objects.get(pk=user_id)

    @database_sync_to_async
    def save_chat_message(self, message, chat, profile):

        chat_message = ChatMessages()
        chat_message.message = message
        chat_message.chat = chat
        chat_message.profile = profile
        chat_message.save()

        return chat_message

        
