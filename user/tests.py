from unittest import skip

from django.test import TestCase
from .views import *


# Create your tests here.

class SignUpTests(TestCase):
    """
    Testing all the fields that require validation.
    Did not test length of the fields of the fields that is stated only in forms.py.
    This is because the length stated in forms.py makes it impossible for the user to write in a longer string.
    """

    def setUp(self):
        ProjectCategory.objects.create(name="Cleaning")
        ProjectCategory.objects.create(name="Painting")
        ProjectCategory.objects.create(name="Gardening")

        self.defaultData = {
            'username': "theUsername",
            'first_name': "theFirstName",
            'last_name': "theLastName",
            'categories': ProjectCategory.objects.all(),  # cleaning
            'company': "theCompany",
            'email': "theEmail@email.com", 'email_confirmation': "theEmail@email.com",
            'password1': "thisisapassword", 'password2': "thisisapassword",
            'phone_number': "12345678",
            'country': "theCountry",
            'state': "theState",
            'city': "theCity",
            'postal_code': "1234",
            'street_address': "theStreetAdress"
        }

    def testUsernameBoundaryLength(self):
        with_shorter_name = self.defaultData
        with_shorter_name["username"] = "t" * 151  # one character longer than allowed
        form = SignUpForm(data=self.defaultData)
        self.assertFalse(form.is_valid())

    def testUserNameBoundaryCharacter(self):
        with_wrong_character = self.defaultData
        with_wrong_character["username"] = "!"
        form = SignUpForm(data=with_wrong_character)
        self.assertFalse(form.is_valid())

    def testEmailNotValid(self):
        with_invalid_email = self.defaultData
        # multiple ways an email can be "invalid". These are not all
        invalid_emails = ["", "a", "a@", "@", "@a", "@.", ".c", "a@.", "@a.", "@.c", "a@a", "a@a.", "a@.c", "@a.c"]

        with_invalid_email["email"] = "h@h.ab"
        with_invalid_email["email_confirmation"] = "h@h.ab"
        form = SignUpForm(data=with_invalid_email)
        self.assertTrue(form.is_valid())

        for notEMail in invalid_emails:
            with_invalid_email["email"] = notEMail
            with_invalid_email["email_confirmation"] = notEMail  # making sure it does not fail on this
            form = SignUpForm(data=with_invalid_email)
            self.assertFalse(form.is_valid())

    @skip('There is no check on if the emails are equal')
    def testEmailConfirmationNotSame(self):
        # Only need to check for if the email is equal on confirmation,
        # as the form will not be valid anyway if first email input fail on syntax
        with_different_email = self.defaultData
        with_different_email["email"] = "a@a.ac"
        with_different_email["email_confirmation"] = "b@a.ac"  # only one character different
        form = SignUpForm(data=with_different_email)
        self.assertFalse(form.is_valid())  # Fails. Apparently there is no check if the emails are equal

    def testPasswordOk(self):
        with_invalid_password = self.defaultData

        # Did not find what was defined as "Your password can't be too similar to your other personal information."
        # or "Your password can't be a commonly used password."
        # So there were no opportunity to test those specifications
        invalid_passwords = ["abcdefg", "12345678"]  # password with -1 characters (7) and entirely numeric
        for not_password in invalid_passwords:
            with_invalid_password["password1"] = not_password
            with_invalid_password["password2"] = not_password
            form = SignUpForm(data=with_invalid_password)
            self.assertFalse(form.is_valid())

    def testPasswordConfirmationEqual(self):
        with_not_simmilar_passwords = self.defaultData
        with_not_simmilar_passwords["password1"] = "heihvordetgar"
        with_not_simmilar_passwords["password2"] = "heihvordetga"
        form = SignUpForm(data=with_not_simmilar_passwords)
        self.assertFalse(form.is_valid())

    def testBoundaryLength(self):
        with_wrong_length = self.defaultData
        # "categories" is not mandatory, but has a limit on 30 characters
        inputs_with_max_30 = ["first_name", "last_name", "categories"]
        inputs_with_max_50 = ["phone_number", "country", "state", "city", "postal_code", "street_address"]
        inputs_with_max_254 = ["email", "email_confirmation"]

        # no value and max + 1
        not_accepted_30 = ["", "a" * 31]
        not_accepted_50 = ["", "a" * 51]
        # Minus six for rest of the email. No need to check empty field, because it is already tested in invalid email
        not_accepted_254 = ["a" * (255 - 6) + "@a.com"]

        for field in inputs_with_max_30:
            for i in range(len(not_accepted_30)):
                with_wrong_length[field] = not_accepted_30[i]
                form = SignUpForm(data=with_wrong_length)
                self.assertFalse(form.is_valid())

        for field in inputs_with_max_50:
            for i in range(len(not_accepted_50)):
                with_wrong_length[field] = not_accepted_50[i]
                form = SignUpForm(data=with_wrong_length)
                self.assertFalse(form.is_valid())

        for field in inputs_with_max_254:
            for i in range(len(not_accepted_254)):
                with_wrong_length[field] = not_accepted_254[i]
                form = SignUpForm(data=with_wrong_length)
                self.assertFalse(form.is_valid())

    def testNoCategories(self):
        with_no_categories = self.defaultData
        with_no_categories["categories"] = None
        form = SignUpForm(data=with_no_categories)
        self.assertFalse(form.is_valid())

    def testDefaultValues(self):
        form = SignUpForm(data=self.defaultData)
        self.assertTrue(form.is_valid())


class SignUp2WayTest(TestCase):
    # doing "country" and "state"

    def setUp(self):
        ProjectCategory.objects.create(name="Cleaning")
        ProjectCategory.objects.create(name="Painting")
        ProjectCategory.objects.create(name="Gardening")
        self.defaultValues = {
            'username': "theUsername",
            'first_name': "theFirstName",
            'last_name': "theLastName",
            'categories': ProjectCategory.objects.all(),  # cleaning
            'company': "theCompany",
            'email': "theEmail@email.com", 'email_confirmation': "theEmail@email.com",
            'password1': "thisisapassword", 'password2': "thisisapassword",
            'phone_number': "12345678",
            'country': "theCountry",
            'state': "theState",
            'city': "theCity",
            'postal_code': "1234",
            'street_address': "theStreetAdress"
        }

    def test(self):
        # The only classification is length, so the combinations are:
        # "", ""
        # "", a
        # "", b * 51
        # a, ""
        # a, a
        # a, b * 51
        # b * 51, ""
        # b * 51, a
        # b * 51, b * 51
        # Since this is combined with boundrey testing, there are nine combinations
        with_alternated_state_country = self.defaultValues
        values = ["", "a", "b" * 51]
        for val1 in values:
            for val2 in values:
                if val1 == values[1] and val2 == values[1]:
                    continue
                with_alternated_state_country["state"] = val1
                with_alternated_state_country["country"] = val2
                form = SignUpForm(data=with_alternated_state_country)
                self.assertFalse(form.is_valid())
