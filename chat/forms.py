from django import forms
from user.models import Profile
from .models import ChatMessages
from django.contrib.auth.models import User

class ChatMessageForm(forms.ModelForm):
    message = forms.CharField(max_length=256, min_length=1, required=True, label='')

    class Meta:
        model = ChatMessages
        fields = ('message', )
